import {CustomRequest} from "../middleware/custom.request";
import {Mongo} from "../mongo";
import {Chunk, ChunkStatus, Submission} from "./submission";
import {UploadChunk} from "../middleware/upload";
import {FilterQuery, ObjectId} from "mongodb";

export const mapRequestForDbUpload = (request: CustomRequest): Submission => {
    return {
        chunks: request.chunks.map(mapRequestChunkForDbUpload),
        submittedAt: request.metadata.submittedAt.toISOString(),
        submittedBy: request.metadata.submittedBy
    };
}

export const mapRequestChunkForDbUpload = (uploadChunk: UploadChunk): Chunk => {
    return {
        property: uploadChunk.propertyCode,
        endingBalance: uploadChunk.endingBalance,
        status: ChunkStatus.IN_PROGRESS
    };
}

export class SubmissionService {

    create = async (request: CustomRequest) => {
        let insertObj = mapRequestForDbUpload(request);
        await Mongo.submissions().insertOne(insertObj);
        return insertObj;
    };

    findAll = async (chunkStatus?: ChunkStatus) => {
        let filterQuery = chunkStatus ? {chunks: {$elemMatch: {status: {$eq: chunkStatus}}}} as FilterQuery<Submission> : undefined;
        return (await Mongo.submissions().find(filterQuery).toArray());
    };

    findOne = async (submissionId: ObjectId) => (await Mongo.submissions().findOne({_id: submissionId}));
}