import {NextFunction, Response} from "express";
import * as fs from "fs";
import {ReadStream} from "fs";
import Papa from "papaparse";
import path from "path";
import {FileColumn, UploadChunk} from "../middleware/upload";
import {CustomRequest} from "../middleware/custom.request";

export const parseFile = async (req: CustomRequest, resp: Response, next: NextFunction) => {
    if (!isCsv(req)) send422(resp, 'Cannot parse file different than .csv')
    try {
        req.chunks = await readStream(req);
        return next();
    } catch (err) {
        // TODO change to winston
        console.log("Error: " + err.message);
        send422(resp, 'Cannot parse file');
    }
}

const parseFileType = (req: CustomRequest) => req.file.originalname.split('.')[1];

const isCsv = (req: CustomRequest) => {
    return parseFileType(req) === 'csv';
}

const readStream = (req: CustomRequest): Promise<UploadChunk[]> => {
    let absolutePath: string = path.resolve(req.file.path);
    let readStream: ReadStream = fs.createReadStream(absolutePath);
    return new Promise<UploadChunk[]>((resolve, reject) => {
        Papa.parse(readStream, {
            header: true,
            complete: results => {
                //TODO how to remove ts-ignore?
                let castResults = results.data.map(next => ({
                    // @ts-ignore
                    propertyCode: next[FileColumn.PROPERTY_CODE],
                    // @ts-ignore
                    endingBalance: next[FileColumn.ENDING_BALANCE],
                    // @ts-ignore
                    postMonth: next[FileColumn.POST_MONTH]
                } as UploadChunk));
                resolve(castResults);
            },
            error: error => reject(error)
        })
    });
}

const send422 = (res: Response, msg: string) => res.status(422).send({error: msg})