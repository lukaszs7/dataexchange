import { ObjectId } from "mongodb";

export interface Submission {
    _id?: ObjectId,
    chunks: Chunk[],
    submittedAt: string,
    submittedBy: string
}

export interface Chunk {
    status: ChunkStatus,
    property: string,
    endingBalance?: number
}

export enum ChunkStatus {
    FAILED,
    IN_PROGRESS,
    COMPLETED
}
