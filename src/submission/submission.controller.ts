import {Controller} from "../controller";
import {Response, Router} from "express";
import {parseFile} from "./submission.middleware";
import {CustomRequest} from "../middleware/custom.request";
import {SubmissionService} from "./submission.service";
import {authorize} from "../auth/auth.middleware";
import {ObjectId} from "mongodb";
import {ChunkStatus} from "./submission";
import {formDataMulter} from "../multer";

// @ts-ignore // TODO how to remove ts-ignore?
export const convertQueryToStatus = (queryStatus: any) => ChunkStatus[queryStatus] as ChunkStatus;

export class SubmissionController implements Controller {
    router = Router();

    constructor(private submissionService: SubmissionService) {
        this.router.post('/submissions', authorize, formDataMulter.single('file'), parseFile, this.createSubmission);
        this.router.get('/submissions', authorize, this.getSubmissions);
        this.router.get('/submissions/:id', authorize, this.getSubmission);
    }

    createSubmission = async (req: CustomRequest, resp: Response) => resp.status(201).json(await this.submissionService.create(req));
    getSubmissions = async (req: CustomRequest, resp: Response) => resp.status(200).json(await this.submissionService.findAll(convertQueryToStatus(req.query.status)));
    getSubmission = async (req: CustomRequest, resp: Response) => resp.status(200).json(await this.submissionService.findOne(new ObjectId(req.params.id)));
}


