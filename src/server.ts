import {config} from "dotenv";

import {Mongo} from "./mongo";
import express from "express";
import {controllers} from "./context";

config();

const {
    MONGO_URL,
    SERVER_PORT
} = process.env;

Mongo.connect(MONGO_URL)
    .then(() => express()
        .use('/', controllers))
    .then(app => app.listen(parseInt(SERVER_PORT), () => {
        // TODO add custom logger
        console.log(`Application started on the port ${SERVER_PORT}`);
    }));