import {Db, MongoClient} from "mongodb";
import {Submission} from "./submission/submission";

export class Mongo {
    public static db: Db;
    public static client: MongoClient;

    public static connect(uri: string): Promise<Db> {
        return new Promise<Db>((resolve, reject) => {
            MongoClient.connect(uri, {useUnifiedTopology: true}, (err, client) => {
                if (err) {
                    reject(err);
                } else {
                    Mongo.client = client;
                    Mongo.db = client.db();
                    resolve(this.db);
                }
            });
        });
    }

    public static disconnect = async () => await Mongo.client.close();
    public static submissions = () => Mongo.db.collection<Submission>('submissions');
}
