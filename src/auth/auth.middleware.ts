import {CustomRequest} from "../middleware/custom.request";
import {NextFunction, Response} from "express";

export const authorize = (req: CustomRequest, resp: Response, next: NextFunction) => {
    // do nothing for now just apply metadata...

    req.metadata = {submittedAt: new Date(), submittedBy: 'Someone'}

    next();
}