export enum FileColumn {
    PROPERTY_CODE = "PROPERTY_CODE",
    ENDING_BALANCE = "ENDING_BALANCE",
    POST_MONTH = "POST_MONTH"
}

export interface UploadChunk {
    propertyCode: string,
    endingBalance: number,
    postMonth: Date
}