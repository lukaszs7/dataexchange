import { Request } from 'express';
import {UploadChunk} from "./upload";

export interface Metadata {
    submittedAt: Date,
    submittedBy: string
}

export interface CustomRequest extends Request {
    metadata: Metadata
    chunks?: UploadChunk[]
}
