import {SubmissionService} from "./submission/submission.service";
import {SubmissionController} from "./submission/submission.controller";

export const submissionService = new SubmissionService();
export const controllers = [
    new SubmissionController(submissionService)
].map(controller => controller.router);
