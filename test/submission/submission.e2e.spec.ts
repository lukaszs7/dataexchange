import {SubmissionService} from "../../src/submission/submission.service";
import {SubmissionController} from "../../src/submission/submission.controller";
import request from 'supertest';
import {Mongo} from "../../src/mongo";
import {createInMemMongo, createTestApp, rmdir, shutDownMongo} from "../utils";

const submissionService = new SubmissionService();
const submissionRouter = new SubmissionController(submissionService);

let app = createTestApp([submissionRouter]);

beforeEach(async () => await createInMemMongo())
afterEach(async () => await Mongo.submissions().deleteMany({}));
afterAll(async () => {
    // clear multer
    await rmdir('tmp')
    // shutdown mongo
    await shutDownMongo()
});

describe('Submissions controller testing', () => {
    it('Create submission', async () => {
        await request(app)
            .post('/submissions')
            .set('Content-type', 'multipart/form-data')
            .attach('file', 'test-resources/test-file.csv')
            .expect(201)
    });

    it('Get submissions', async (done) => {
        request(app)
            .post('/submissions')
            .attach('file', 'test-resources/test-file.csv')
            .end(() => {
                request(app)
                    .get('/submissions')
                    .expect(200)
                    .end((err, res) => {
                        let expectedChunk = {
                            "endingBalance": "3",
                            "property": "p123",
                            "status": 1
                        };
                        expect(res.body[0]._id).not.toBeNull();
                        expect(res.body[0].submittedAt).not.toBeNull();
                        expect(res.body[0].chunks[0]).toEqual(expectedChunk);
                        expect(res.body[0].submittedBy).toEqual('Someone');
                        done();
                    })
            });
    })
});
