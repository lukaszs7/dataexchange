import express from "express";
import {Mongo} from "../src/mongo";
import {Controller} from "../src/controller";

import {MongoMemoryServer} from 'mongodb-memory-server';
import * as fs from "fs";

let mongoInMemory: MongoMemoryServer;

export const createInMemMongo = async () => {
    mongoInMemory = new MongoMemoryServer();
    let uri = await mongoInMemory.getUri();
    await Mongo.connect(uri);
}

export const shutDownMongo = async () => {
    await Mongo.disconnect()
    if (mongoInMemory) {
        await mongoInMemory.stop();
    }
}

export const createTestApp = (routers: Controller[]) => express().use('/', routers.map(c => c.router));
export const rmdir = (dir: string) =>
    fs.rmdir(dir, {recursive: true}, () => console.log(`Purged all files from ${dir} directory`));
