import {authorize} from "../../src/auth/auth.middleware";
import {CustomRequest} from "../../src/middleware/custom.request";
import {Response} from "express";

const req = {
    chunks: []
} as CustomRequest;

const resp = {} as Response;

const next = jest.fn();

describe('Auth testing', () => {
    it('should enhance metadata', () => {
        authorize(req, resp, next);

        expect(req.metadata).not.toBeNull();
        expect(req.metadata.submittedBy).toBe('Someone');
        expect(req.metadata.submittedAt).not.toBeNull();
        expect(next).toHaveBeenCalled();
    });
});